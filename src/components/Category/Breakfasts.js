import React from "react";
import { Header, Segment, Card, Image } from "semantic-ui-react";

const ListBreakfasts = [
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Papa a la huancaina",
    rating: "34",
    metaContent: "El fogón criollo",
    price: "S/24.00"
  },
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Arroz con pollo",
    rating: "0",
    metaContent: "Mi piachi restobar",
    price: "S/14.00"
  },
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Lomo saltado",
    rating: "12",
    metaContent: "El saltado de la abuela",
    price: "S/22.00"
  },
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Caldo criollo",
    rating: "0",
    metaContent: "Mi piachi restobar",
    price: "S/16.00"
  },
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Lomo saltado",
    rating: "12",
    metaContent: "El saltado de la abuela",
    price: "S/22.00"
  }
];

export default function Breakfasts(props) {
  return (
    <Segment style={{ padding: "0em 0em" }} basic>
      <Header as="h3" style={{ fontWeight: "550" }}>
        {props.title}
      </Header>
      <span
        style={{
          display: "flex",
          alignItems: "row",
          justifyContent: "column",
          marginBottom: "0px"
        }}
      >
        {ListBreakfasts.map((items, key) => (
          <Segment
            key={key}
            style={
              key === 0
                ? {
                    paddingRight: "1em",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    paddingLeft: "0px",
                    marginTop: "0px"
                  }
                : { padding: "0em 1em", marginTop: "0px" }
            }
            basic
          >
            <Card style={{ width: "220px" }}>
              <Image
                src={items.image}
                style={{ width: "220px", height: "150px" }}
                centered
              />
            </Card>
          </Segment>
        ))}
      </span>
    </Segment>
  );
}
