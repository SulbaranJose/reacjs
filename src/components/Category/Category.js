import React from "react";
import { Header, Segment, Card, Image, Rating } from "semantic-ui-react";

const ListCategory = [
  {
    image: require("../../styles/images/image2.png"),
    Title: "Desayunos"
  },
  {
    image: require("../../styles/images/image4.png"),
    Title: "Almuerzos"
  },
  {
    image: require("../../styles/images/image5.png"),
    Title: "Postres y antojos",
    disabled: true
  }
];

export default function Category(props) {
  return (
    <Segment style={{ padding: "0em 0em" }} basic>
      <Header as="h3" style={{ fontWeight: "550" }}>
        Categorías
      </Header>
      <span
        style={{
          display: "flex",
          alignItems: "row",
          justifyContent: "column"
        }}
      >
        {ListCategory.map((items, key) => (
          <Segment
            key={key}
            style={
              key === 0
                ? {
                    paddingRight: "1em",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    paddingLeft: "0px",
                    marginTop: "0px"
                  }
                : { padding: "0em 1em", marginTop: "0px" }
            }
            basic
            disabled={items.disabled ? true : false}
          >
            <Segment
              style={{
                marginBottom: "0px",
                display: "flex",
                alignItems: "row",
                justifyContent: "column",
                padding: "0em 0em",
                width: "300px",
                borderColor: items.disabled ? "" : "#F5AB3E"
              }}
            >
              <Image
                src={items.image}
                style={{ width: "107px", height: "71px" }}
              />
              <Header as="h5" style={{ marginLeft: "1em" }}>
                <span>{items.Title}</span>
              </Header>
            </Segment>
          </Segment>
        ))}
      </span>
    </Segment>
  );
}
