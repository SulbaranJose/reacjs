import React from "react";
import { Header, Grid, Input, Icon } from "semantic-ui-react";
import { NavLink } from "react-router-dom";

export default function MySearch(props) {
  return (
    <Grid columns={2}>
      <Grid.Column>
        <Header as="h6" style={{ fontWeight: "200" }}>
          SAN BORJA SUR 754 (Ubicación actual){" "}
          <NavLink to="#">
            <span style={{ color: "orange" }}> Cambiar </span>
          </NavLink>
        </Header>
      </Grid.Column>
      <Grid.Column textAlign="right">
        <Icon name="sort amount down" size="big" />
        <Input icon="search" iconPosition="left" placeholder="" />
      </Grid.Column>
    </Grid>
  );
}
