import React from "react";
import { Header, Segment, Card, Image, Rating } from "semantic-ui-react";

const ListRecomend = [
  {
    image: require("../../styles/images/imagen1.png"),
    metaTitle: "Papa a la huancaina",
    rating: "34",
    metaContent: "El fogón criollo",
    price: "S/24.00"
  },
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Arroz con pollo",
    rating: "0",
    metaContent: "Mi piachi restobar",
    price: "S/14.00"
  },
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Lomo saltado",
    rating: "12",
    metaContent: "El saltado de la abuela",
    price: "S/22.00"
  },
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Caldo criollo",
    rating: "0",
    metaContent: "Mi piachi restobar",
    price: "S/16.00"
  },
  {
    image: "https://react.semantic-ui.com/images/wireframe/image.png",
    metaTitle: "Lomo saltado",
    rating: "12",
    metaContent: "El saltado de la abuela",
    price: "S/22.00"
  }
];

export default function Recommended(props) {
  return (
    <Segment style={{ padding: "0em 0em" }} basic>
      <Header as="h3" style={{ fontWeight: "550" }}>
        Recomendados para ti
      </Header>
      <span
        style={{
          display: "flex",
          alignItems: "row",
          justifyContent: "column",
          marginBottom: "0px"
        }}
      >
        {ListRecomend.map((items, key) => (
          <Segment
            key={key}
            style={
              key === 0
                ? {
                    paddingRight: "1em",
                    paddingTop: "0px",
                    paddingBottom: "0px",
                    paddingLeft: "0px",
                    marginTop: "0px"
                  }
                : { padding: "0em 1em", marginTop: "0px" }
            }
            basic
          >
            <Card style={{ width: "220px" }}>
              <Image
                src={items.image}
                style={{ width: "220px", height: "150px" }}
                centered
              />
              <Card.Content>
                <Header
                  as="h5"
                  style={{
                    marginBottom: "0px",
                    display: "flex",
                    alignItems: "row",
                    justifyContent: "space-between"
                  }}
                >
                  <span>{items.metaTitle}</span>
                  <span>
                    <Rating
                      size="mini"
                      icon="heart"
                      defaultRating={0}
                      maxRating={1}
                    />
                    <span style={{ marginLeft: "-10px", fontSize: "11px" }}>
                      {items.rating}
                    </span>
                  </span>
                </Header>
                <Card.Meta
                  style={{
                    display: "flex",
                    alignItems: "row",
                    justifyContent: "space-between"
                  }}
                >
                  <span>{items.metaContent}</span>
                  <span>{items.price}</span>
                </Card.Meta>
              </Card.Content>
            </Card>
          </Segment>
        ))}
      </span>
    </Segment>
  );
}
