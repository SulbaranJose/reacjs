import React, { useState } from "react";
//import * as ROUTES from '../../constants/routers';
import { Segment, Form, Icon, Image, Button, Input } from "semantic-ui-react";

const src = "https://react.semantic-ui.com/images/wireframe/image.png";

export default function AddRestaurantFrom(props) {
  const { title } = props;
  const [imagesSelected, setImagesSelected] = useState([]);
  const [restaurantName, setRestaurantName] = useState("");
  const [restaurantAddress, setRestaurantAddress] = useState("");
  const [restaurantDescription, setRestaurantDescription] = useState("");
  const [isVisibleMap, setIsVisibleMap] = useState(false);
  const [locationRestaurant, setLocationRestaurant] = useState(null);

  return (
    <>
      <ImageRestaurant imageRestaurant={imagesSelected[0]} />
      <FormAdd
        setRestaurantName={setRestaurantName}
        setRestaurantAddress={setRestaurantAddress}
        setRestaurantDescription={setRestaurantDescription}
        setIsVisibleMap={setIsVisibleMap}
        locationRestaurant={locationRestaurant}
      />
      <Button positive style={{ marginTop: "1em" }}>
        Crear Restaurante
      </Button>
    </>
  );
}

function ImageRestaurant(props) {
  const { imageRestaurant } = props;
  return (
    <Segment basic style={{ padding: "0em, 0em" }}>
      {imageRestaurant ? (
        <Image
          source={{ uri: imageRestaurant }}
          size="medium"
          centered
          bordered
        />
      ) : (
        <Image
          src="https://react.semantic-ui.com/images/wireframe/white-image.png"
          size="medium"
          centered
          bordered
        />
      )}
    </Segment>
  );
}

function FormAdd(props) {
  const {
    setRestaurantName,
    setRestaurantAddress,
    setRestaurantDescription,
    setIsVisibleMap,
    locationRestaurant
  } = props;

  return (
    <Form size="small">
      <Form.Input
        autoComplete="off"
        fluid
        type="text"
        placeholder="Nombre del restaurante"
        required={true}
        name=""
        onChange={(e, { value }) => setRestaurantName(value)}
      />
      <Form.Input
        autoComplete="off"
        fluid
        placeholder="Dirección"
        type="text"
        required={true}
        icon={
          <Icon
            inverted
            style={{ color: locationRestaurant ? "#00a680" : "#c2c2c2" }}
            name="map marker alternate"
          />
        }
        onChange={(e, { value }) => setRestaurantAddress(value)}
      />
      <Form.TextArea
        autoComplete="off"
        type="text"
        placeholder="Descripcion del restaurante"
        required={true}
        readOnly={true}
        onChange={(e, { value }) => setRestaurantDescription(value)}
      />
    </Form>
  );
}
