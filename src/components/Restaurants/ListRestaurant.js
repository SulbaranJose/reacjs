import PropTypes from "prop-types";
import React from "react";
//import * as ROUTES from '../../constants/routers';

import { Card, Segment, Icon, Image, Button } from "semantic-ui-react";

const src = "https://react.semantic-ui.com/images/wireframe/image.png";

export default function ListRestaurant(props) {
  const { title } = props;
  return (
    <Segment raised>
      <Segment as={Card} centered link raised>
        <Segment inverted color="teal" textAlign="center">
          <Segment
            inverted
            color="teal"
            textAlign="right"
            style={{ padding: "0em 0em" }}
          >
            <Icon name="setting" color="black" size="large" />
          </Segment>
          <Image src={src} size="tiny" verticalAlign="middle" />
          <span> {title} </span>
          <Segment basic style={{ padding: "0em 0em" }}>
            <Button color="blue" fluid>
              Seleccionar
            </Button>
          </Segment>
        </Segment>
      </Segment>
    </Segment>
  );
}

ListRestaurant.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string
};
