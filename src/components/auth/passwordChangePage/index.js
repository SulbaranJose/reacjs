import React, { Component } from 'react';
import { Button, Form } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { withFirebase } from '../../../firebase';

const INITIAL_STATE = {
  code: '',
  password: '',
  password2: '',
  error: null,
};
class PasswordChangePage extends Component {
  state = { ...INITIAL_STATE };

  onSubmit = event => {
    event.preventDefault();
    //let { code, password, password2 } = this.state;
  };
  render() {
    const { code, error, password, password2 } = this.state;
    const isInvalid = password !== password2 || code ==='';
    return (
        <Form size='small' error onSubmit={this.onSubmit}>
            <Form.Input
              autoComplete='off'
              fluid 
              icon='user' 
              iconPosition='left' 
              type="number" 
              placeholder='Código' 
              required={true}
              onChange={( event, newValue ) => this.setState({ code:newValue.value })}
              value = {code}
            />
            <Form.Input
              autoComplete='off'
              fluid
              icon='lock'
              iconPosition='left'
              placeholder='Contraseña'
              type='password'
              required={true}
              onChange={( event, newValue ) => this.setState({ password:newValue.value })}
              value = {password}
            />
            <Form.Input
              autoComplete='off'
              fluid
              icon='lock'
              iconPosition='left'
              placeholder='Repetir contraseña'
              type='password'
              required={true}
              onChange={( event, newValue ) => this.setState({ password2:newValue.value })}
              value = {password2}
            />
            <Button type='submit' disabled={isInvalid} color='brown' fluid>
               CONTINUAR
            </Button>
          {error && <p>{error.message}</p>}
        </Form>

    );
  }
}

const PasswordChangeForm = compose(
  withRouter,
  withFirebase
)(PasswordChangePage);

export default PasswordChangeForm;
