import React, { Component, Fragment } from 'react';
import * as COLOR from '../../../constants';
import * as FIREBASE_ERRORS from '../../../constants/firebase/errors';
import { Form } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { withFirebase } from '../../../firebase';
import { Messages, Buttons } from '../../../content';

const INITIAL_STATE = {
  email: '',
  error: null,
  message: null
};
class PasswordForgetPage extends Component {
  state = { ...INITIAL_STATE };

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
      message: null, 
      error:null
    });
  };
  onSubmit = event => {
    event.preventDefault();
    let { email } = this.state
    this.props.firebase
    .doPasswordReset(email)
    .then(authUser => {
       this.setState({  ...INITIAL_STATE });
       this.setState({ message:'Se a enviado un enlace a tu correo electrónico.' });
     })   
    .catch(error => {
      if (error.code && error.code === FIREBASE_ERRORS.auth__user_not_found){
        this.setState({ message: null, error: FIREBASE_ERRORS.auth__user_not_found_meg});
      }
    });

    //this.props.history.replace(routes.PASSWORD_FORGET+'/'+email);
  };
  render() {
    const { email, error, message } = this.state;
    const isInvalid = email === '';
    return (
      <Fragment>
        {error   && <Messages color = {COLOR.RED} message = {error} />}
        {message && <Messages color = {COLOR.GREEN} message = {message} />}
        <Form size='small' error onSubmit={this.onSubmit}>
            <Form.Input
              autoComplete='off'
              fluid 
              icon='mail' 
              iconPosition='left' 
              type="email" 
              placeholder='Correo electrónico' 
              required={true}
              name='email'
              onChange={this.handleChange}
              value = {email}
            />
            <Buttons type='submit' disabled={isInvalid}>
               RESTABLECER
            </Buttons>
        </Form>
      </Fragment>
    );
  }
}

const PasswordForgetForm = compose(
  withRouter,
  withFirebase
)(PasswordForgetPage);

export default PasswordForgetForm;
