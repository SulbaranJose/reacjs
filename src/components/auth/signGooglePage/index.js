import React, { Component } from 'react';
import { Button, Form, Icon } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { withFirebase } from '../../../firebase';

const INITIAL_STATE = {
  error: null
};

class SignGoogleFormBase extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    event.preventDefault();
    this.props.firebase
      .doSignInWithGoogle()
      .then(socialAuthUser => {
        this.props.firebase
          .user(socialAuthUser.user.uid)
          .set({
            username: socialAuthUser.user.displayName,
            email: socialAuthUser.user.email,
            roles: {},
          });
          let { routes } = this.props
          this.props.history.replace(routes.DASHBOARD);
      })
      .then(socialAuthUser => {
        this.setState({ error: null });
        //this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  render() {
    const { error } = this.state;
    return (
        <Form style={{marginTop:'10px'}}  error onSubmit={this.onSubmit}>
          <Button type="submit"  color='grey' fluid>
            <Icon name='google plus' /> Google
          </Button>
          {error && <p>{error.message}</p>}
        </Form>
    );
  }
}

const SignGoogleForm = compose(
  withRouter,
  withFirebase,
)(SignGoogleFormBase);

export default  SignGoogleForm;
