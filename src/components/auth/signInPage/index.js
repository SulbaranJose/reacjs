import React, { Component, Fragment } from "react";
import * as COLOR from "../../../constants";
import * as FIREBASE_ERRORS from "../../../constants/firebase/errors";
import { Form } from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { withFirebase } from "../../../firebase";
import { Messages, Buttons } from "../../../content";

const INITIAL_STATE = {
  email: "",
  password: "",
  error: null
};
class SignInFormBase extends Component {
  state = { ...INITIAL_STATE };

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
      message: null,
      error: null
    });
  };
  onSubmit = event => {
    event.preventDefault();
    let { email, password } = this.state;
    this.props.firebase
      .doSignIn(email, password)
      .then(response => {
        localStorage.setItem("SignIn", true);
        let { routes } = this.props;
        this.setState({ ...INITIAL_STATE });
        this.props.history.replace(routes.DASHBOARD);
      })
      .catch(error => {
        if (error.code && error.code === FIREBASE_ERRORS.auth__user_not_found) {
          this.setState({
            message: null,
            error: FIREBASE_ERRORS.auth__user_not_found_meg
          });
        }
        if (error.code && error.code === FIREBASE_ERRORS.auth__wrong_password) {
          this.setState({
            message: null,
            error: FIREBASE_ERRORS.auth__wrong_password_meg
          });
        }
      });
  };
  render() {
    const { email, password, error } = this.state;
    const isInvalid = email === "" || password === "";
    return (
      <Fragment>
        {error && <Messages color={COLOR.RED} message={error} />}
        <Form size="small" error onSubmit={this.onSubmit}>
          <Form.Input
            autoComplete="off"
            fluid
            icon="user"
            iconPosition="left"
            type="email"
            placeholder="Correo electrónico"
            required={true}
            name="email"
            onChange={this.handleChange}
            value={email}
          />
          <Form.Input
            autoComplete="off"
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Contraseña"
            type="password"
            required={true}
            name="password"
            onChange={this.handleChange}
            value={password}
          />
          <Buttons type="submit" disabled={isInvalid}>
            ENTRAR
          </Buttons>
        </Form>
      </Fragment>
    );
  }
}

const SignInForm = compose(withRouter, withFirebase)(SignInFormBase);

export default SignInForm;
