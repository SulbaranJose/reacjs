import React, { Component } from 'react';
import { Button,  Message, Form } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { withFirebase } from '../../../firebase';


class SignOutBase extends Component {
  constructor(props) {
    super(props);
  }

  onSubmit = event => {
    event.preventDefault();
    this.props.firebase
      .doSignOut()
      .then(socialAuthUser => {
          console.log(socialAuthUser)
      })
      .then(socialAuthUser => {
        this.setState({ error: null });
        //this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  render() {
    const { error } = this.state;
    return (
        <Form size='large' error onSubmit={this.onSubmit}>
          <Message>
            <Button type="submit" circular color='google plus' icon='google plus' />
          </Message>
          {error && <p>{error.message}</p>}
        </Form>

    );
  }
}

const SignGoogleForm = compose(
  withRouter,
  withFirebase,
)(SignGoogleFormBase);

export default  SignGoogleForm;