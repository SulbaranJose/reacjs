import React, { Component, Fragment } from 'react';
import * as FIREBASE_ERRORS from '../../../constants/firebase/errors';
import * as setUserInfo from '../../../store/actions/menu'
import axios from 'axios'
import { Button } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import { compose } from 'recompose'
import { withFirebase } from '../../../firebase'
import { connect } from 'react-redux'

const INITIAL_STATE = {
  error: null
};

class SignSocialNetwork extends Component {
  state = { ...INITIAL_STATE };
  listener = null;

  onSubmit = event => {
    event.preventDefault();
    this.listener = this.props.firebase
      .doSignInWithGoogle()
      .then(socialAuthUser => {
        let { additionalUserInfo } = socialAuthUser;
        let { email, family_name, given_name, picture, id } = additionalUserInfo.profile;
        let { user } = socialAuthUser;
        let { routes, endpoint } = this.props;
        let body = {
          email,
          last_name:family_name,
          first_name:given_name,
          photo_url:picture,
          google_id:id,
          uid:user.uid,
          display_name: `${family_name} ${given_name}`
        };
        let { setUserInfo } = this.props
        if ( additionalUserInfo.isNewUser === true ){
          axios.post(endpoint.api.auth.signup_google,body)
          .then(response => {
              let { msg, success } = response.data;
              if (success === 'true'){
                this.setState({ ...INITIAL_STATE });
                localStorage.setItem("SignIn",true)
                setUserInfo({ userInfo:{
                    photoURL: socialAuthUser.user.photoURL,
                    name:socialAuthUser.user.displayName
                  }
                })
                this.props.history.replace(routes.DASHBOARD);
              } else {
                console.log(msg);
                //this.setState({ error: { message:msg } });
              }
           })
        } else {
          setUserInfo({ userInfo:{
              photoURL: socialAuthUser.user.photoURL,
              name:socialAuthUser.user.displayName
            }
          })
          localStorage.setItem("SignIn",true)
          this.props.history.replace(routes.DASHBOARD);
        }
      })
      .catch(error => {
        if (error.code && error.code === FIREBASE_ERRORS.auth__popup_closed_by_user){
          this.setState({ error: null});
        }
      });
  };

  render() {
    const { error } = this.state;
    return (
        <Fragment>
          {error && <p>{error.message}</p>}
          <Button circular disabled={true} color='facebook' icon='facebook' />
          <Button circular disabled={true} color='twitter' icon='twitter' />
          <Button circular disabled={true} color='linkedin' icon='linkedin' />
          <Button circular color='google plus' icon='google plus' onClick={this.onSubmit} />
        </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setUserInfo: id => dispatch(setUserInfo.userInfo(id))
})

const SignSocialNetworkBotton = compose(
  withRouter,
  withFirebase,
  connect(null, mapDispatchToProps)
)(SignSocialNetwork);

export default  SignSocialNetworkBotton;
