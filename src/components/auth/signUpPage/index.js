import React, { Component, Fragment } from "react";
import * as COLOR from "../../../constants";
import { Form } from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { withFirebase } from "../../../firebase";
import { Messages, Buttons } from "../../../content";
import { api } from "../../../constants/endpoint";
import axios from "axios";

const INITIAL_STATE = {
  firstName: "",
  lastName: "",
  email: "",
  password: "",
  password2: "",
  error: null,
  message: null
};

class SignUpForm extends Component {
  state = { ...INITIAL_STATE };

  handleChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
      message: null,
      error: null
    });
  };
  onSubmit = event => {
    event.preventDefault();
    const { firstName, lastName, email, password } = this.state;
    let body = {
      first_name: firstName,
      last_name: firstName,
      email,
      password,
      display_name: `${firstName} ${lastName}`
    };

    axios
      .post(api.auth.signup, body)
      .then(response => {
        let { msg, success } = response.data;
        if (success === "true") {
          this.setState({ ...INITIAL_STATE });
          this.setState({ message: msg });
        } else {
          this.setState({ error: { message: msg } });
        }
      })
      .catch(error => {
        console.log(error.response);
      });
  };
  render() {
    const {
      firstName,
      lastName,
      email,
      password,
      password2,
      error,
      message
    } = this.state;
    const isInvalid =
      password !== password2 ||
      email === "" ||
      firstName === "" ||
      lastName === "" ||
      password === "" ||
      password2 === "";

    return (
      <Fragment>
        {error && <Messages color={COLOR.RED} message={error.message} />}
        {message && <Messages color={COLOR.GREEN} message={message} />}
        <Form size="small" error onSubmit={this.onSubmit}>
          <Form.Input
            autoComplete="off"
            fluid
            icon="user"
            iconPosition="left"
            type="text"
            placeholder="Primer nombre"
            name="firstName"
            required={true}
            onChange={this.handleChange}
            value={firstName}
          />
          <Form.Input
            autoComplete="off"
            fluid
            icon="user"
            iconPosition="left"
            type="text"
            placeholder="Segundo nombre"
            required={true}
            name="lastName"
            onChange={this.handleChange}
            value={lastName}
          />
          <Form.Input
            autoComplete="off"
            fluid
            icon="mail"
            iconPosition="left"
            type="email"
            placeholder="Correo electrónico"
            required={true}
            name="email"
            onChange={this.handleChange}
            value={email}
          />
          <Form.Input
            autoComplete="off"
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Contraseña"
            type="password"
            required={true}
            name="password"
            onChange={this.handleChange}
            value={password}
          />
          <Form.Input
            autoComplete="off"
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Repetir contraseña"
            type="password"
            required={true}
            name="password2"
            onChange={this.handleChange}
            value={password2}
          />
          <Buttons type="submit" disabled={isInvalid}>
            REGISTRAR
          </Buttons>
        </Form>
      </Fragment>
    );
  }
}

const SignUpFormBase = compose(withRouter, withFirebase)(SignUpForm);

export default SignUpFormBase;
