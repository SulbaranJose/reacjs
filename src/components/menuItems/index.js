import PropTypes from "prop-types";
import React, { Component, Fragment } from "react";
import * as ROUTES from "../../constants/routers";
import * as setMenuItem from "../../store/actions/menu";
import { getMenuItem } from "../../store/selectors";
import { Menu } from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";

class menuItems extends Component {
  activeItemsMenu = items => {
    this.props.setMenuItem({ items: items });
    switch (items) {
      case "LS":
        return this.props.history.push(ROUTES.LIST);
      case "C5TA":
        return this.props.history.push(ROUTES.CALCULATION);
      case "REST_LIST":
        return this.props.history.push(ROUTES.RESTAURANTS_LIST);
      default:
        return "foo";
    }
  };

  render() {
    const { items } = this.props;
    return (
      <Fragment>
        <Menu.Item
          as="a"
          active={items === "REST_LIST"}
          onClick={() => this.activeItemsMenu("REST_LIST")}
        >
          Restaurantes
        </Menu.Item>
        <Menu.Item
          as="a"
          active={items === "LS"}
          onClick={() => this.activeItemsMenu("LS")}
        >
          Listado
        </Menu.Item>
        <Menu.Item
          as="a"
          active={items === "C5TA"}
          onClick={() => this.activeItemsMenu("C5TA")}
        >
          Calculo de 5ta
        </Menu.Item>
      </Fragment>
    );
  }
}
menuItems.propTypes = {
  style: PropTypes.object
};

const mapStateToProps = state => ({
  items: getMenuItem(state)
});

const mapDispatchToProps = dispatch => ({
  setMenuItem: id => dispatch(setMenuItem.menuItems(id))
});

const MenuItems = compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(menuItems);

export default MenuItems;
