import PropTypes from "prop-types";
import React, { Component, Fragment } from "react";
import * as ROUTES from "../../constants/routers";
import * as setUserInfo from "../../store/actions/menu";
import { Dropdown, Image, Icon, Header } from "semantic-ui-react";
import { withFirebase } from "../../firebase";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { getUserInfo } from "../../store/selectors";
import { connect } from "react-redux";

class OptionsProfile extends Component {
  handleSignOut = async () => {
    let { setUserInfo } = this.props;
    await this.props.firebase.doSignOut();
    await localStorage.clear();
    setUserInfo({ userInfo: false });
    this.props.history.replace(ROUTES.ROOT);
  };

  options = items => {
    switch (items) {
      case "PLARET-SETTING":
        return ""; //this.props.history.push(ROUTES.SETTING);
      /*case 'USER-ACCOUNT':
        return this.props.history.replace(ROUTES.USER_ACCOUNT)
      case 'USER-SETTING':
        return this.props.history.replace(ROUTES.USER_SETTING)*/
      default:
        return "";
    }
  };
  render() {
    const { style, userInfo } = this.props;
    return (
      <Fragment>
        <span>
          <Image
            avatar
            floated="right"
            style={{ width: "25px", height: "25px" }}
            src={
              userInfo && userInfo.photoURL
                ? userInfo.photoURL
                : "https://react.semantic-ui.com/images/avatar/large/steve.jpg"
            }
          />
        </span>
        <span>
          <Header
            as="h5"
            inverted
            content={userInfo.name}
            style={{ marginLeft: "5px", fontWeight: "400" }}
          />
        </span>
        <Dropdown
          trigger={
            <span>
              <Icon name="ellipsis vertical" style={{ marginLeft: "5px" }} />
            </span>
          }
          pointing="top right"
          icon={null}
          style={style}
        >
          <Dropdown.Menu>
            <Dropdown.Header content={userInfo.name} />
            <Dropdown.Divider />
            <Dropdown.Item
              text="Account"
              icon="user"
              onClick={() => this.options("USER-ACCOUNT")}
            />
            <Dropdown.Item
              text="Settings"
              icon="settings"
              onClick={() => this.options("USER-SETTING")}
            />
            <Dropdown.Item
              text="Sign Out"
              icon="sign out"
              onClick={this.handleSignOut}
            />
          </Dropdown.Menu>
        </Dropdown>
      </Fragment>
    );
  }
}
OptionsProfile.propTypes = {
  style: PropTypes.object
};
const mapStateToProps = state => ({
  userInfo: getUserInfo(state)
});
const mapDispatchToProps = dispatch => ({
  setUserInfo: id => dispatch(setUserInfo.userInfo(id))
});

const OptionsProfileUser = compose(
  withRouter,
  withFirebase,
  connect(mapStateToProps, mapDispatchToProps)
)(OptionsProfile);

export default OptionsProfileUser;
