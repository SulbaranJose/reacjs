export const url = '/';

//export const base = 'http://mangostaweb.com:8070';
export const base = 'https://mangostaweb.com:8087';
export const api = {
	auth: {
		signup: `${base}/user`,
		signup_google:`${base}/user`
	}
}