export const auth__user_not_found = 'auth/user-not-found';
export const auth__wrong_password = 'auth/wrong-password';
export const auth__popup_closed_by_user = 'auth/popup-closed-by-user';


export const auth__user_not_found_meg = 'No existe ningún registro de usuario que corresponda al identificador proporcionado.';
export const auth__wrong_password_meg = 'La contraseña no es válida o el usuario no tiene una contraseña.';
export const auth__popup_closed_by_user_meg = 'La ventana emergente ha sido cerrada por el usuario antes de finalizar la operación';
