export const ROOT = "/";
// ======= AUTH ====== //
export const SIGN_IN = ROOT + "signin";
export const SIGN_UP = ROOT + "signup";
export const PASSWORD_FORGET = ROOT + "password-forget";
export const PASSWORD_CHANGE = PASSWORD_FORGET + "/:username";
// ======= DASHBOARD ====== //
export const DASHBOARD = ROOT + "dashboard";

export const LIST = `${ROOT}list`;
export const CALCULATION = `${ROOT}calculation`;

// ======= RESTAURANTS ====== //
const RESTAURANTS = "restaurants";
export const RESTAURANTS_LIST = `${ROOT}${RESTAURANTS}`;
export const RESTAURANTS_ADD = `${ROOT}${RESTAURANTS}/new`;

// ======= USER-ACCOUNT ====== //
const USER = "user";
export const USER_ACCOUNT = `${ROOT}${USER}/account`;
export const USER_SETTING = `${ROOT}${USER}/setting`;
