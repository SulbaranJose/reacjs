import PropTypes from "prop-types";
import React from "react";
import { Message, Button, Container, Header } from "semantic-ui-react";

export const Messages = props => (
  <Message floating size="mini" color={props.color}>
    {props.message}
  </Message>
);

export const Buttons = props => (
  <Button
    type={props.type ? props.type : "submit"}
    disabled={props.disabled ? props.disabled : false}
    color="orange"
    fluid
  >
    {props.children}
  </Button>
);

export const ContainerPage = ({ list, title, children, className }) => (
  <Container
    style={title ? { padding: "3em 3em" } : { padding: "0em 3em" }}
    className={className}
  >
    {title ? (
      <Header
        as="h3"
        style={
          list ? { fontSize: "2em" } : { fontSize: "2em", paddingBottom: "1em" }
        }
      >
        {title}
      </Header>
    ) : null}
    {children}
  </Container>
);
ContainerPage.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  className: PropTypes.string,
  list: PropTypes.bool
};
