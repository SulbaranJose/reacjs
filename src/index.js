import React from "react";
import ReactDOM from "react-dom";
import App from "./navigation";
import * as serviceWorker from "./serviceWorker";
import Firebase, { FirebaseContext } from "./firebase";
import "./styles/styles.css";
import "react-notifications/lib/notifications.css";
ReactDOM.render(
  <FirebaseContext.Provider value={new Firebase()}>
    <App />
  </FirebaseContext.Provider>,
  document.getElementById("root")
);
serviceWorker.unregister();
