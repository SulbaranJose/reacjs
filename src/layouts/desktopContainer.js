import PropTypes from "prop-types";
import React from "react";
import OptionsProfileUser from "../components/profileUser";
import MenuItems from "../components/menuItems";
import { Menu, Responsive, Sidebar, Header } from "semantic-ui-react";

export default function DesktopContainer(props) {
  const { children } = props;
  return (
    <Responsive minWidth={Responsive.onlyTablet.minWidth}>
      <Menu fixed="top" inverted secondary size="small" color="orange">
        <Menu.Item>QueHaYCerca</Menu.Item>
        <Menu.Item position="right">
          <OptionsProfileUser style={{ marginRight: "0.5em" }} />
        </Menu.Item>
      </Menu>
      <div style={{ marginTop: "40px" }}>{children}</div>
    </Responsive>
  );
}

DesktopContainer.propTypes = {
  children: PropTypes.node
};
