import PropTypes from "prop-types";
import React, { Fragment } from "react";
import DesktopContainer from "./desktopContainer";
import MobileContainer from "./mobileContainer";
import { withAuthentication } from "../session";

const PageContainer = ({ children }) => (
  <Fragment>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </Fragment>
);

PageContainer.propTypes = {
  children: PropTypes.node
};

export default withAuthentication(PageContainer);
