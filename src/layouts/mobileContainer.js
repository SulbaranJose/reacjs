import PropTypes from "prop-types";
import React, { useState } from "react";
import OptionsProfileUser from "../components/profileUser";
import {
  Menu,
  Sidebar,
  Segment,
  Header,
  Icon,
  Responsive
} from "semantic-ui-react";
import MenuItems from "../components/menuItems";

export default function MobileContainer(props) {
  const { children } = props;
  const [visible, setVisible] = useState(false);
  return (
    <Responsive maxWidth={Responsive.onlyMobile.maxWidth}>
      <Sidebar.Pushable as={Segment} style={{ padding: "0em 0em" }} basic>
        <Sidebar
          as={Menu}
          animation="overlay"
          icon="labeled"
          inverted
          onHide={() => setVisible(false)}
          vertical
          visible={visible}
          style={{
            minHeight: "50vh",
            width: "275px"
          }}
          color="orange"
          direction="left"
        >
          <Menu.Item>
            <Header as="h3"> Que hay cerca </Header>
          </Menu.Item>
          <MenuItems style={{}} />
        </Sidebar>
        <Sidebar.Pusher dimmed={visible}>
          <Menu fixed="top" inverted size="small" color="orange">
            <Menu.Item onClick={() => setVisible(true)}>
              <Icon name="sidebar" />
            </Menu.Item>
            <Menu.Item position="right">
              <OptionsProfileUser />
            </Menu.Item>
          </Menu>
          <Segment basic>{children}</Segment>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    </Responsive>
  );
}

MobileContainer.propTypes = {
  children: PropTypes.node
};
