import React from "react";
import * as ROUTES from "../constants/routers";
import store from "../store";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
  // Autn
  ViewSignIn,
  ViewSignUp,
  ViewPasswordForget,
  //Dashboard
  ViewDashboard
} from "../views";

import { List } from "../views/list";

import { Calculation } from "../views/calculation";

import { Restaurants, AddRestaurant } from "../views/restaurants";

//import PasswordChange from "../views/auth/passwordChange";

const NotFound = () => (
  <div>
    <br />
    <center>
      <h3>404 Pagina no encontrada</h3>
      <p>Lo sentimos, pero la página que busca no existe.</p>
    </center>
  </div>
);

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path={ROUTES.ROOT} component={ViewSignIn} />
          <Route path={ROUTES.SIGN_IN} component={ViewSignIn} />
          <Route path={ROUTES.SIGN_UP} component={ViewSignUp} />
          {/*<Route path={ROUTES.PASSWORD_CHANGE} component={PasswordChange} />*/}
          <Route path={ROUTES.PASSWORD_FORGET} component={ViewPasswordForget} />
          <Route exact path={ROUTES.DASHBOARD} component={ViewDashboard} />
          <Route exact path={ROUTES.LIST} component={List} />
          <Route exact path={ROUTES.CALCULATION} component={Calculation} />
          <Route exact path={ROUTES.RESTAURANTS_LIST} component={Restaurants} />
          <Route
            exact
            path={ROUTES.RESTAURANTS_ADD}
            component={AddRestaurant}
          />
          <Route render={NotFound} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
