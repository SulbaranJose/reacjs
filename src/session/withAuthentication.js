import React, { Fragment } from 'react'
import { withFirebase } from '../firebase'
import * as ROUTES from '../constants/routers'
import * as setUserInfo from '../store/actions/menu'
import { getUserInfo } from '../store/selectors'
import { compose } from 'redux'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

let condition = () => !!localStorage.SignIn;

const withAuthentication = Component => {
  class WithAuthenticationComponent extends React.Component {
    state = {
        authUser: null
    };
    componentDidMount() {
      this.listener = this.props.firebase.auth.onAuthStateChanged(
        authUser => {
          if (authUser) {
            if(!condition()){
              localStorage.setItem("SignIn", true);
            }
            let { userInfo, setUserInfo } = this.props
            if (userInfo === false) {
              setUserInfo({ userInfo :{
                photoURL:authUser.photoURL,
                name:authUser.displayName
              }})
            }
            this.setState({ authUser })
          } else {
            localStorage.clear()
            this.props.history.replace(ROUTES.SIGN_IN)
          }
        }
      )
    }
    componentWillUnmount() {
      this.listener();
    }
    render() {
      const { authUser } = this.state
      return (
        <Fragment>
          { authUser !== null ? <Component { ...this.props } /> : null }
        </Fragment>
      )
    }
  }
  const mapStateToProps = state => ({
    userInfo: getUserInfo(state)
  })

  const mapDispatchToProps = dispatch => ({
    setUserInfo: id => dispatch(setUserInfo.userInfo(id))
  })
  const WithAuthentication = compose(
    withRouter,
    withFirebase,
    connect(mapStateToProps, mapDispatchToProps)
  )(WithAuthenticationComponent);

  return withFirebase(WithAuthentication);
};

export default withAuthentication;