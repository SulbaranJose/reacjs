import React, { Fragment } from 'react';
import { withFirebase } from '../firebase';
import * as ROUTES from '../constants/routers';

let condition = () => !!localStorage.SignIn;

const withNotAuthentication = Component => {
  class WithNotAuthentication extends React.Component {
    state = {
        authNotUser: null
    };
    componentDidMount() {
      if (!condition()) {
        this.listener = this.props.firebase.auth.onAuthStateChanged(
          authUser => {
            if (authUser) {
              localStorage.setItem('SignIn', true)
              this.props.history.replace(ROUTES.DASHBOARD);
            } else {
              localStorage.clear()
              this.setState({ authNotUser: true });
            }
          }
        );
      } else {
        this.props.history.replace(ROUTES.DASHBOARD);
      }
    }
    componentWillUnmount() {
      let { authNotUser } = this.state;
      if (authNotUser !== null) {
        this.listener();
      }
    }
    render() {
      const { authNotUser } = this.state;
      return (
        <Fragment>
          { authNotUser !== null ? <Component { ...this.props } /> : null }
        </Fragment>
      )
    }
  }
  return withFirebase(WithNotAuthentication);
};

export default withNotAuthentication;
