export const MENU_ITEMS = 'MENU_ITEMS'
export const MENU_INFO_USER = 'MENU_INFO_USER'

const menuItems = text => ({
  type:MENU_ITEMS,
  data: text
})

const userInfo = text => ({
  type:MENU_INFO_USER,
  data: text
})

export {
  menuItems,
  userInfo
}
