import axios from 'axios'

const methodGet = (url, headers={}) =>
  axios.get(url, headers)
  .then(response => response)
  .catch(error => error.response)

const methodPost = (url, query, headers={}) =>
  axios.post(url, query, headers)
  .then(response => response)
  .catch(error => error.response)