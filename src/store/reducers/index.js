import { combineReducers } from 'redux'
import menuReducer from './menu'

const rootReducer = combineReducers({
  menuState: menuReducer
});

export default rootReducer;
