import * as ACTIONS_MENU from '../../actions/menu'

const initialState = {
  items:'home',
  userInfo:false
}

function menuReducer(state=initialState, action) {
  switch (action.type) {
    case ACTIONS_MENU.MENU_ITEMS:
      return {
        ...state,
        items: action.data.items
      }
    case ACTIONS_MENU.MENU_INFO_USER:
      return {
        ...state,
        userInfo: action.data.userInfo
      }
    default:
      return state;
  }
}
export default menuReducer;
