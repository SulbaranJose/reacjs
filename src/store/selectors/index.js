const getMenuItem = ({ menuState }) =>
  menuState.items

const getUserInfo = ({ menuState }) =>
  menuState.userInfo

export {
  getMenuItem,
  getUserInfo
}