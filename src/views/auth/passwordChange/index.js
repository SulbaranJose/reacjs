import React from "react";
import PasswordChangeForm from "../../../components/auth/passwordChangePage";
import {
  Card,
  Message,
  Container,
  Header,
  Segment,
  Grid
} from "semantic-ui-react";
import * as ROUTES from "../../../constants/routers";
import { NavLink } from "react-router-dom";

const styles = {
  brown: {
    color: "brown"
  },
  padding: {
    padding: "0px, 5em"
  }
};
const PasswordChange = () => (
  <Grid>
    <Grid.Column as={Segment} basic>
      <Card centered className="workborder">
        <Container>
          <Segment basic textAlign="center">
            <Header as="h1" color="orange" content="QUE HAY CERCA" />
            <Header as="h3" color="black" content="Restablecer contraseña" />
            <PasswordChangeForm routes={ROUTES} />
            <Message as={Segment} basic>
              <NavLink to={ROUTES.SIGN_IN}>
                <span style={styles.brown}> Iniciar sesión </span>
              </NavLink>
            </Message>
          </Segment>
        </Container>
      </Card>
    </Grid.Column>
  </Grid>
);
export default PasswordChange;
