import React from "react";
import PasswordForgetForm from "../../../components/auth/passwordForgetPage";
import * as ROUTES from "../../../constants/routers";
import {
  Card,
  Message,
  Container,
  Header,
  Segment,
  Grid
} from "semantic-ui-react";
import { withNotAuthentication } from "../../../session";
import { compose } from "redux";
import { NavLink } from "react-router-dom";

const styles = {
  teal: {
    color: "teal"
  },
  padding: {
    padding: "0px, 5em"
  }
};
const PasswordForget = () => (
  <Grid>
    <Grid.Column as={Segment} basic>
      <Card centered className="workborder">
        <Container>
          <Segment basic textAlign="center">
            <Header as="h1" color="orange" content="QUE HAY CERCA" />
            <Header as="h3" color="black" content="Restablecer contraseña" />
            <Header as="h4">
              <Header.Subheader>
                Ingresa el correo electrónico y te enviaremos un mensaje
              </Header.Subheader>
            </Header>
            <PasswordForgetForm routes={ROUTES} />
            <Message as={Segment} basic>
              <NavLink to={ROUTES.SIGN_IN}>
                <span style={styles.teal}> Iniciar sesión </span>
              </NavLink>
            </Message>
          </Segment>
        </Container>
      </Card>
    </Grid.Column>
  </Grid>
);

const ViewPasswordForget = compose(withNotAuthentication)(PasswordForget);

export default ViewPasswordForget;
