import React from "react";
import * as ROUTES from "../../../constants/routers";
import * as ENDPOINT from "../../../constants/endpoint";
import SignInForm from "../../../components/auth/signInPage";
//import SignGoogleForm from '../../../components/auth/signGooglePage'
import SignSocialNetworkBotton from "../../../components/auth/signSocialNetwork";
import { withNotAuthentication } from "../../../session";
import { compose } from "redux";
import {
  Grid,
  Header,
  Segment,
  Message,
  Divider,
  Card
} from "semantic-ui-react";
import { NavLink } from "react-router-dom";

const styles = {
  padding: {
    padding: "0px"
  },
  message1: {
    background: "rgba(1, 1, 1, 0.69)",
    height: "100%",
    padding: "0px",
    borderRadius: "0px"
  },
  message2: {
    background: "black",
    padding: "0px",
    borderRadius: "0px"
  },
  header_color: {
    color: "#fff",
    marginBottom: "10px"
  },
  grey: {
    color: "grey"
  },
  teal: {
    color: "teal"
  },
  segment: {
    //5em
    padding: "5.5em 1em"
  }
};

const SignIn = () => {
  return (
    <Grid columns={2} stackable textAlign="center">
      <Grid.Column as={Segment} basic>
        <Card centered className="workborder">
          <Segment basic style={styles.segment}>
            <Header
              as="h1"
              color="orange"
              content="QUE HAY CERCA"
              style={{ marginBottom: "50px" }}
            />
            <SignInForm routes={ROUTES} />
            {/*<SignGoogleForm routes = { ROUTES } />*/}
            <Message as={Segment} basic>
              <NavLink to={ROUTES.PASSWORD_FORGET}>
                <span style={styles.teal}> ¿Olvidaste tu contraseña? </span>
              </NavLink>
            </Message>
            <Divider inverted />
            <Message as={Segment} basic>
              <span as="h1" style={styles.grey}>
                ¿No tienes cuenta?
              </span>
              <NavLink to={ROUTES.SIGN_UP}>
                <span style={styles.teal}> Crea tu cuenta </span>
              </NavLink>
            </Message>
            <SignSocialNetworkBotton routes={ROUTES} endpoint={ENDPOINT} />
          </Segment>
        </Card>
      </Grid.Column>
      <Grid.Column
        as={Segment}
        basic
        className="landing-image"
        style={styles.padding}
      ></Grid.Column>
    </Grid>
  );
};

const ViewSignIn = compose(withNotAuthentication)(SignIn);

export default ViewSignIn;
