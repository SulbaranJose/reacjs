import React from "react";
import SignUpForm from "../../../components/auth/signUpPage";
import SignSocialNetworkBotton from "../../../components/auth/signSocialNetwork";
import { withNotAuthentication } from "../../../session";
import { compose } from "redux";
import {
  Card,
  Message,
  Container,
  Header,
  Divider,
  Segment,
  Grid
} from "semantic-ui-react";
import * as ROUTES from "../../../constants/routers";
import * as ENDPOINT from "../../../constants/endpoint";
import { NavLink } from "react-router-dom";

const styles = {
  teal: {
    color: "teal"
  }
};
const SignUp = () => (
  <Grid>
    <Grid.Column as={Segment} basic>
      <Card centered className="workborder">
        <Container>
          <Segment basic textAlign="center">
            <Header as="h1" color="orange" content="QUE HAY CERCA" />
            <Header as="h3" color="black" content="Nuevo usuario" />
            <SignUpForm routes={ROUTES} />
            <Message as={Segment} basic>
              <NavLink to={ROUTES.SIGN_IN}>
                <span style={styles.teal}> Iniciar sesión </span>
              </NavLink>
            </Message>
            <Divider inverted />
            <SignSocialNetworkBotton routes={ROUTES} endpoint={ENDPOINT} />
          </Segment>
        </Container>
      </Card>
    </Grid.Column>
  </Grid>
);

const ViewSignUp = compose(withNotAuthentication)(SignUp);

export default ViewSignUp;
