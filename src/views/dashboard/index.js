import React from "react";
import { Segment } from "semantic-ui-react";
import PageContainer from "../../layouts";
import Recommended from "../../components/Category/Recommended";
import Category from "../../components/Category/Category";
import Breakfasts from "../../components/Category/Breakfasts";
import MySearch from "../../components/Category/MySearch";

export default function HomepageLayout(props) {
  return (
    <PageContainer>
      <Segment style={{ padding: "2em 3em" }} basic>
        <MySearch />
        <Recommended />
        <Category />
        <Breakfasts title="Desayunos" />
        <Breakfasts title="Almuerzos" />
        <Breakfasts title="Productos más gustados" />
        <Breakfasts title="Productos destacados" />
      </Segment>
    </PageContainer>
  );
}
