import ViewSignIn from "./auth/signIn";
import ViewSignUp from "./auth/signUp";
import ViewPasswordForget from "./auth/passwordForget";
import ViewDashboard from "./dashboard";

export {
  // Autn
  ViewSignIn,
  ViewSignUp,
  ViewPasswordForget,
  // Dashboard
  ViewDashboard
}