import React from "react";
import PageContainer from "../../layouts";
import { Table, Container, Segment } from "semantic-ui-react";
import { ContainerPage } from "../../content";

const List = () => (
  <PageContainer>
    <ContainerPage title="Listado" className="page-title list">
      <Container className="myOverflow" style={{ marginLeft: "0em" }}>
        <Segment raised style={{ padding: "0em 0em" }}>
          <Table celled unstackable selectable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Nombre Apellido</Table.HeaderCell>
                <Table.HeaderCell>DNI o CE</Table.HeaderCell>
                <Table.HeaderCell>Cargo</Table.HeaderCell>
                <Table.HeaderCell>F. Nac</Table.HeaderCell>
                <Table.HeaderCell>F. Ing</Table.HeaderCell>
                <Table.HeaderCell>Cent. Costos</Table.HeaderCell>
                <Table.HeaderCell>Sist. Pensión</Table.HeaderCell>
                <Table.HeaderCell>CUSPP</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
              <Table.Row>
                <Table.Cell>John</Table.Cell>
                <Table.Cell>123456789</Table.Cell>
                <Table.Cell>Desarrollador WEB</Table.Cell>
                <Table.Cell>17/02/98</Table.Cell>
                <Table.Cell>28/07/2010</Table.Cell>
                <Table.Cell>Oficona 3</Table.Cell>
                <Table.Cell>Habitat</Table.Cell>
                <Table.Cell>2919313</Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>
        </Segment>
      </Container>
    </ContainerPage>
    <Container style={{ padding: "5em 0em" }}> </Container>
  </PageContainer>
);

export default List;
