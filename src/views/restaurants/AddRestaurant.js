import React, { useState, useRef } from "react";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
import PageContainer from "../../layouts";
import { ContainerPage } from "../../content";

import AddRestaurantFrom from "../../components/Restaurants/AddRestaurantFrom";

export default function AddRestaurant(props) {
  const { history } = props;
  const toastRef = useRef();
  const [isLoading, setIsLoading] = useState(false);
  return (
    <PageContainer>
      <ContainerPage
        title="Nuevo restaurant"
        className="page-title"
        list={true}
      >
        <AddRestaurantFrom
          toastRef={toastRef}
          setIsLoading={setIsLoading}
          navigation={history}
        />
        <NotificationContainer />
      </ContainerPage>
    </PageContainer>
  );
}
