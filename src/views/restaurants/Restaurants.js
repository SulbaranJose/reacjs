import React, { useState } from "react";
import ListRestaurant from "../../components/Restaurants/ListRestaurant";
import PageContainer from "../../layouts";
import { Header, Grid, Tab, Icon, Segment } from "semantic-ui-react";
import { ContainerPage } from "../../content";

const PanesTab = ({ title }) => (
  <>
    <Header size="small">{title}</Header>
    <Grid columns={3} doubling stackable>
      <Grid.Column>
        <ListRestaurant title="Restaurante-1" />
      </Grid.Column>
      <Grid.Column>
        <ListRestaurant title="Restaurante-2" />
      </Grid.Column>
      <Grid.Column>
        <ListRestaurant title="Restaurante-3" />
      </Grid.Column>
    </Grid>
  </>
);

export default function Restaurants(props) {
  const addRestaurants = () =>
    props.history.replace(`${props.location.pathname}/new`);
  return (
    <PageContainer>
      <ContainerPage title="Restaurantes" className="page-title" list={true}>
        <Segment
          basic
          style={{
            padding: "0em 0em",
            display: "flex",
            alignItems: "row",
            justifyContent: "flex-end",
            marginTop: "0em"
          }}
        >
          <Icon
            circular
            inverted
            style={{
              margin: "0em 0em"
            }}
            color="teal"
            name="add"
            link
            onClick={addRestaurants}
          />
        </Segment>
        <Tab
          menu={{ secondary: true, pointing: true }}
          className="tab-overflow"
          panes={[
            {
              menuItem: "Configuración",
              render: () => <PanesTab title="Configuración" />
            },
            {
              menuItem: "Seleccione Restaurantes",
              render: () => <PanesTab title="Seleccione Restaurantes" />
            },
            {
              menuItem: "Usuarios",
              render: () => <PanesTab title="Usuarios" />
            },
            {
              menuItem: "Histórico",
              render: () => <PanesTab title="Histórico" />
            }
          ]}
        />
      </ContainerPage>
    </PageContainer>
  );
}
