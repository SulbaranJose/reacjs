import Restaurants from "./Restaurants";
import AddRestaurant from "./AddRestaurant";

export { Restaurants, AddRestaurant };
